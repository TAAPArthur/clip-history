# Clip History

Keeps a list of everything copied.

# Install

```
make
make install
```

# Usage
```
clip-history monitor # listen for changes
clip-history select # interactivly select entry and copy into the clipboard
```
