#include <X11/Xatom.h>
#include <X11/Xlib.h>
#include <X11/extensions/Xfixes.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argv, const char** args) {
    Display* dpy;
    Window root;
    Atom clip;
    XEvent event;
    int event_base, error_base;
    dpy = XOpenDisplay(NULL);
    if(!dpy) {
        fprintf(stderr, "Can't open X display\n");
        exit(1);
    }
    root = DefaultRootWindow(dpy);
    XFixesQueryExtension(dpy, &event_base, &error_base);
    int n = 1;
    int noLoop = 0;
    if(argv > n)
        if(strcmp("--no-loop", args[n]) == 0) {
            n++;
            noLoop = 0;
        }
    int i;
    for(i = n; i < argv; i++) {
        char buffer[32];
        if(args[i][0] == '-' && args[i][1] == '-' && args[i][2] == 0) {
            i++;
            break;
        }
        for(int j = 0; j < sizeof(buffer) - 1 && args[i][j]; j++)
            buffer[j] = toupper(args[i][j]);
        XFixesSelectSelectionInput(dpy, root, XInternAtom(dpy, buffer, False), XFixesSetSelectionOwnerNotifyMask);
    }
    if(i == n) {
        clip = XInternAtom(dpy, "CLIPBOARD", False);
        XFixesSelectSelectionInput(dpy, root, XA_PRIMARY, XFixesSetSelectionOwnerNotifyMask);
        XFixesSelectSelectionInput(dpy, root, XA_SECONDARY, XFixesSetSelectionOwnerNotifyMask);
        XFixesSelectSelectionInput(dpy, root, clip, XFixesSetSelectionOwnerNotifyMask);
    }
    do {
        XNextEvent(dpy, &event);
        XFixesSelectionNotifyEvent* selectionEvent = (XFixesSelectionNotifyEvent*)&event;
        const char* str;
        if (selectionEvent->selection == XA_PRIMARY) {
            str = "primary";
        } else if(selectionEvent->selection == XA_SECONDARY) {
            str = "secondary";
        } else {
            str = "clipboard";
        }
        if(args[i]) {
            setenv("selection", str, 1);
            system(args[i]);
        } else {
            printf("%ld %s\n", selectionEvent->selection, str);
            fflush(NULL);
        }
    } while(!noLoop);
    XCloseDisplay(dpy);
}
